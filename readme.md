    ||||||||||||||||||||||||||||||||||||||||||||||||||||||||||
    |||██╗|||██╗███████╗|||███████╗|█████╗|███████╗███████╗|||
    |||██║|||██║██╔════╝|||██╔════╝██╔══██╗██╔════╝██╔════╝|||
    |||██║|||██║█████╗|||||███████╗███████║███████╗███████╗|||
    |||╚██╗|██╔╝██╔══╝|||||╚════██║██╔══██║╚════██║╚════██║|||
    ||||╚████╔╝|██║||||||||███████║██║||██║███████║███████║|||
    |||||╚═══╝||╚═╝||||||||╚══════╝╚═╝||╚═╝╚══════╝╚══════╝|||
    ||||||||||||||||||||||||||||||||||||||||||||||||||||||||||
---

##The no-conflict Bootstrap base built for VisualForce pages

+ Supports standard Salesforce stylesheets
+ Loads of veriables for fast site-wide adjustments
+ Ready to be zipped and used a static-resource


####Development prerequisites:
+ Bower
+ LibSass or Ruby Sass


####Packages (managed by Bower):
+ Bootstrap Sass - 3.3.6
+ jQuery - 2.2.0


####License
MIT License - See license.txt

---

####Please note: Editing the the css files directly is a bad life choice if you plan on compiling ever again.
If you must do such a thing, create a separate file to prevent issues when the base stylesheets are compiled.
With the exception of some third-party plug-ins or frameworks, all of the CSS has been compiled from the .scss or .styl files located in `/stylesheets` folder.





